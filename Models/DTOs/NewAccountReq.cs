using System.ComponentModel.DataAnnotations;

namespace NewTwitterCloneApi.Models.DTOs
{
    public class NewAccountReq
    {
        [Required]
        [MaxLength(20)]
        public string AccountId {get; set;}

        [Required]
        [MaxLength(50)]
        public string FullName {get; set;}

        [Required]
        public string Email {get; set;}

        [Required]
        public int DayOfBirth {get; set;}

        [Required]
        public int MonthOfBirth {get; set;}

        [Required]
        public int YearOfBirth {get; set;}

        [Required]
        public string Password {get; set;}
    }
}