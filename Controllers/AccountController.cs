using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewTwitterCloneApi.Attributes;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Data;
using NewTwitterCloneApi.Models;
using NewTwitterCloneApi.Models.DTOs;
using NewTwitterCloneApi.Utilities;

namespace NewTwitterCloneApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        public AppDbContext _appDbContext;

        private SupportController _supportController;

        public AccountController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _supportController = new SupportController(appDbContext);
        }

        [ValidateModel]
        [HttpPost("create-new-account")]
        public async Task<ActionResult<Account>> CreateNewAccount([FromBody]NewAccountReq newAccountReq)
        {
            var selectedAccount = await _supportController.GetAccountById(newAccountReq.AccountId);
            bool AccountIdExists = _supportController.GetAccountExists(selectedAccount);
            if(AccountIdExists)
            {
                return BadRequest("already exists");
            }

            var newAccount = GetAccountFromNewAccountReq(newAccountReq);
            await _appDbContext.Accounts.AddAsync(newAccount);
            await _appDbContext.SaveChangesAsync();
            
            return Ok(newAccount);
        }

        [ValidateModel]
        [HttpPost("login-account")]
        public async Task<IActionResult> Login([FromBody]LoginAccountReq account)
        {
            var selectedAccount = await _supportController.GetAccountById(account.AccountId);
            bool AccountIdExists = _supportController.GetAccountExists(selectedAccount);
            if(!AccountIdExists)
            {
                return BadRequest("account does not exist");
            }

            bool AccountBlacklisted = _supportController.GetBlacklistStatus(selectedAccount);
            if(AccountBlacklisted)
            {
                return BadRequest("You have been blacklisted");
            }

            bool PasswordIsCorrect = _supportController.GetPasswordIsCorrect(account, selectedAccount);
            if(!PasswordIsCorrect)
            {
                return BadRequest("incorrect password");
            }

            AttachCredentialsToHeader(account.AccountId);
            return Ok();
        }

        [Auth]
        [HttpGet("get-my-account")]
        public async Task<ActionResult<Account>> GetAccount()
        {
            string accessToken   =  Request.Headers[HeaderTypes.AccessToken];
            string accountId    =   TokenUtility.ExtractAccountIdFromToken(accessToken);

            var account =   await _supportController.GetAccountById(accountId);
            bool AccountIdExists = _supportController.GetAccountExists(account);
            if(!AccountIdExists)
            {
                return BadRequest();
            }

            return Ok(account);
        }

        [Auth]
        [HttpPut("change-my-password")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordReq changePwdObject)
        {
            string AccessToken  = Request.Headers[HeaderTypes.AccessToken];
            string AccountId    = TokenUtility.ExtractAccountIdFromToken(AccessToken);
            
            var selectedAccount = await _supportController.GetAccountById(AccountId);
            var accountExists   = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }
            var oldHashedPassword   = selectedAccount.HashedPassword;
            bool oldPasswordsMatch  = HashUtility.GetPasswordsMatch(
                                        changePwdObject.OldPassword,
                                        oldHashedPassword
                                    );
            if(oldPasswordsMatch)
            {
                string newHashedPwd = HashUtility.GetHashedPassword(changePwdObject.NewPassword);
                selectedAccount.HashedPassword  = newHashedPwd;
                _appDbContext.SaveChanges();
                return Ok();
            }

            return BadRequest("old pwds don't match");
        }

        [Auth]
        [HttpPut("update-my-account")]
        public async Task<ActionResult<Account>> UpdateMyAccount([FromBody]UpdateAccountReq accountUpdate)
        {
            string AccessToken  = Request.Headers[HeaderTypes.AccessToken];
            string AccountId    = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var selectedAccount = await _supportController.GetAccountById(AccountId);
            var accountExists   = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            UpdateAccountProperties(selectedAccount, accountUpdate);
            _appDbContext.SaveChanges();

            return Ok(selectedAccount);
        }

        [Auth]
        [HttpGet("get-all-accounts")]
        public ActionResult<List<Account>> GetAllAccounts()
        {
            var accountsNotEmpty = _appDbContext.Accounts.Any();
            if(!accountsNotEmpty)
            {
                return Ok("empty");
            }
            return Ok(_appDbContext.Accounts);
        }

        private void UpdateAccountProperties(Account account, UpdateAccountReq accountUpdate)
        {
            account.FullName    = accountUpdate.FullName;
            account.Email       = accountUpdate.Email;
            account.PhotoUrl    = accountUpdate.PhotoUrl;
            account.Bio         = accountUpdate.Bio;
        }

        private Account GetAccountFromNewAccountReq(NewAccountReq newAccountReq)
        {
            return new Account()
            {
                AccountId       =   newAccountReq.AccountId,
                FullName        =   newAccountReq.FullName,
                Email           =   newAccountReq.Email,
                DateOfBirth     =   TimeUtility.GetDateTimeFromDate(
                    newAccountReq.DayOfBirth,
                    newAccountReq.MonthOfBirth,
                    newAccountReq.YearOfBirth
                ),
                Bio             =   "",
                PhotoUrl        =   "",
                CreatedAt       =   DateTime.UtcNow,
                Type            =   AccountTypes.Basic,
                HashedPassword  =   HashUtility.GetHashedPassword(newAccountReq.Password)
            };
        }

        private async void AttachCredentialsToHeader(string accountId)
        {
            var credentialsRes = await _supportController.GetCredentialsRes(accountId);
            Response.Headers.Add(HeaderTypes.AccessToken, credentialsRes.AccessToken);
            Response.Headers.Add(HeaderTypes.RefreshToken, credentialsRes.RefreshToken);
        }

    }
}