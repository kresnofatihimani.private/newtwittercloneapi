using System;
using NewTwitterCloneApi.Constants;

namespace NewTwitterCloneApi.Utilities
{
    public static class HashUtility
    {
        public static string GetSaltedPassword(string text)
        {
            return text + Salt.SaltString;
        }

        public static string GetHashedPassword(string text)
        {
            string saltedPassword = GetSaltedPassword(text);
            return BCrypt.Net.BCrypt.HashPassword(saltedPassword);
        }

        public static bool GetPasswordsMatch(string loginPassword, string hashedPassword)
        {
            var saltedLoginPassword = GetSaltedPassword(loginPassword);
            return BCrypt.Net.BCrypt.Verify(saltedLoginPassword, hashedPassword);
        }
    }
}