namespace NewTwitterCloneApi.Constants
{
    public static class HeaderTypes
    {
        public static readonly string AccessToken = "tc-access-token";

        public static readonly string RefreshToken = "tc-refresh-token";
    }
}