namespace NewTwitterCloneApi.Constants
{
    public static class RelationTypes
    {
        public static readonly string Following = "Following";

        public static readonly string Block = "Block";
    }
}