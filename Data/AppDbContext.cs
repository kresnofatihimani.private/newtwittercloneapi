using Microsoft.EntityFrameworkCore;
using NewTwitterCloneApi.Models;

namespace NewTwitterCloneApi.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext>options) : base(options)
        {
            //
        }

        public DbSet<Account> Accounts {get; set;}

        public DbSet<Relation> Relations {get; set;}

        public DbSet<Tweet> Tweets {get; set;}
    }
}