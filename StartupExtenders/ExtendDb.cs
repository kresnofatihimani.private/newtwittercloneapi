using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewTwitterCloneApi.Data;

namespace NewTwitterCloneApi.StartupExtenders
{
    public static class ExtendDb
    {
        public static void AddTwitterDbOptions(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<AppDbContext>(opt=>
                opt.UseNpgsql(config.GetConnectionString("myPostgresDbConnection"))
            );
        }
    }
}