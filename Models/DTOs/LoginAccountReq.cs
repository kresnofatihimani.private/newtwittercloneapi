using System.ComponentModel.DataAnnotations;

namespace NewTwitterCloneApi.Models.DTOs
{
    public class LoginAccountReq
    {
        [Required]
        public string AccountId {get; set;}

        [Required]
        public string Password {get; set;}
    }
}