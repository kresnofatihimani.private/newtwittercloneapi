using System.ComponentModel.DataAnnotations;

namespace NewTwitterCloneApi.Models.DTOs
{
    public class UpdateAccountReq
    {
        [Required]
        [MaxLength(50)]
        public string FullName {get; set;}

        [Required]
        [MaxLength(50)]
        public string Email {get; set;}

        public string PhotoUrl {get; set;}

        public string Bio {get; set;}
    }
}