using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace NewTwitterCloneApi.Models
{
    public class Account
    {
        [Key]
        [Required]
        [MaxLength(20)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string AccountId { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName {get; set;}

        [Required]
        public string Email {get; set;}

        public string PhotoUrl {get; set;}

        [Required]
        public DateTime DateOfBirth {get; set;}

        [MaxLength(100)]
        public string Bio {get; set;}

        [Required]
        [JsonIgnore]
        public string HashedPassword {get; set;}

        [Required]
        [MaxLength(20)]
        public string Type {get; set;}

        [Required]
        public DateTime CreatedAt {get; set;}
    }
}