using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace NewTwitterCloneApi.Attributes
{
    public class ValidateModelAttribute   :   ActionFilterAttribute
    {
        public ValidateModelAttribute()
        {
            //
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if(!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult("Model Not Valid");
            }
        }
    }
}