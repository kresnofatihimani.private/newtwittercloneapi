using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Utilities;

namespace NewTwitterCloneApi.Attributes
{
    public class AuthAttribute : ActionFilterAttribute
    {
        private string _tokenClaimType;

        private string _value;

        public AuthAttribute()
        {
            //
        }

        public AuthAttribute(string tokenClaimType, string value)
        {
            _tokenClaimType = tokenClaimType;
            _value          = value;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string accessToken = context.HttpContext.Request.Headers[HeaderTypes.AccessToken];
            string refreshToken = context.HttpContext.Request.Headers[HeaderTypes.RefreshToken];
            if(accessToken == null || refreshToken == null)
            {
                context.Result = new BadRequestObjectResult("incomplete header");
            }
            else
            {
                bool tokenSignIsValid = ValidateSignature(accessToken);
                bool refreshTokenSignIsValid = ValidateSignature(refreshToken);
                
                if(!tokenSignIsValid || !refreshTokenSignIsValid)
                {
                    context.Result = new BadRequestObjectResult("not authed");
                }
                else
                {
                    if(_tokenClaimType != null && _value != null)
                    {
                        bool claimIsValid = ValidateClaim(accessToken);
                        if(!claimIsValid)
                        {
                            context.Result = new BadRequestObjectResult("claim not valid");
                        }
                        else
                        {
                            HandleTokens(context, accessToken, refreshToken);
                        }
                    }
                    else
                    {
                        HandleTokens(context, accessToken, refreshToken);
                    }
                }
            }
        }

        private bool ValidateSignature(string jwtstring)
        {
            var handler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            try
            {
                handler.ValidateToken(jwtstring, TokenValidationParams.Params, out validatedToken);
            }
            catch
            {
                return false;
            }

            return validatedToken != null;
        }

        private bool ValidateClaim(string accessToken)
        {
            string extractedClaimValue = TokenUtility.ExtractClaimValueFromToken(accessToken, _tokenClaimType);
            return (extractedClaimValue == _value);
        }

        private bool ValidateExpiration(string jwtstring)
        {
            long expireTime = TokenUtility.ExtractExpireTimeFromToken(jwtstring);
            if(expireTime>TimeUtility.GetNowSecondsUtc())
            {
                return false;   // has not expired
            }

            return true;    // has expired
        }

        private void HandleTokens(ActionExecutingContext context, string accessToken, string refreshToken)
        {
            bool accessTokenIsExpired = ValidateExpiration(accessToken);
            bool refreshTokenIsExpired = ValidateExpiration(refreshToken);
            if(refreshTokenIsExpired)
            {
                if(accessTokenIsExpired)
                {
                    context.Result = new BadRequestObjectResult("session expired");
                }
                else
                {
                    var newRefreshToken = TokenUtility.RefreshRefreshToken(refreshToken);
                    context.HttpContext.Response.Headers.Add(HeaderTypes.AccessToken, accessToken);
                    context.HttpContext.Response.Headers.Add(HeaderTypes.RefreshToken, newRefreshToken);
                }
            }
            else
            {
                if(accessTokenIsExpired)
                {
                    var newAccessToken = TokenUtility.RefreshAccessToken(refreshToken);
                    context.HttpContext.Response.Headers.Add(HeaderTypes.AccessToken, newAccessToken);
                    context.HttpContext.Response.Headers.Add(HeaderTypes.RefreshToken, refreshToken);
                }
                else
                {
                    context.HttpContext.Response.Headers.Add(HeaderTypes.AccessToken, accessToken);
                    context.HttpContext.Response.Headers.Add(HeaderTypes.RefreshToken, refreshToken);
                }
            }

        }
    }
}